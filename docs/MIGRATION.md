# v5 Migration guide

* Rename `.babelrc` to `babel.config.json` and change the `extends` property to match.
```json
{
  "extends": "./node_modules/bm-web-build/babel.config.json"
}
```
* Update following `scripts` in `package.json`
```json
{
  "scripts": {
    "lint:css": "stylelint 'app_modules/**/*.{vue,htm,html,css,sss,less,scss,sass}' --config stylelint.config.js",
    "build": "node node_modules/bm-web-build/build/build-app.js",
    "report": "env config=localhost npm_config_report=true node node_modules/bm-web-build/build/build-app.js",
    "start": "webpack-dev-server --progress --config node_modules/bm-web-build/build/webpack.app-dev.conf.js",
    ...
  }
}
```
* Update `jsconfig.json` for resolving aliases by IDE
```json
{
  "compilerOptions": {
    "baseUrl": "./",
    "paths": {
      "@app/*": ["app_modules/*"],
      "@assets/*": ["assets/*"],
      "@static/*": ["static/*"]
    }
  },
  "exclude": [
    "dist",
    "node_modules"
  ]
}
```
* Update web-workers usage according to [docs](../README.md#markdown-header-web-workers)

## How to use less

update stylelint.config.js file in the root, for correct linting
```javascript
module.exports = { // eslint-disable-line import/no-commonjs
  extends: 'stylelint-config-bemobile/vue',
  overrides: [
    {
      files: ['**/*.less'],
      customSyntax: 'postcss-less',
    },
  ],
};
```

## Migration Notes

* `chalk`, v4 is used because latter version uses ES modules
* `ora`, v5 is used because latter version uses ES modules
* `UglifyJsPlugin` is replaced by `terser-webpack-plugin`
* `OptimizeCSSPlugin` is replaced by `CssMinimizerPlugin`
* `CommonsChunkPlugin` is replaced by `SplitChunksPlugin`
* `HashedModuleIdsPlugin` is removed
* `ModuleConcatenationPlugin` is removed, it includes for production by default
* `ScriptExtHtmlWebpackPlugin` is removes, `HtmlWebpackPlugin` handles defer attribute out of the box
* `WebpackRuntimePublicPathPlugin` is replaced by `webpack-authoring-libraries` https://webpack.js.org/guides/author-libraries/#root


