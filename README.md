# bm-web-build

This package contains the development and production build scripts for our Vue web applications and packages.

**Note**: For packages, see the "Building packages" section in particular; most of this guide does not concern packages.

**bm-web-build** also features some configuration files that applications should inherit from.

Lastly, it includes the dependencies required for our ESLint and StyleLint configs, so that apps don't have to list these in their own dependencies.

All of this is configured out of the box for new projects created using **vue-start-template**, but it's a good idea to read through this documentation anyway.

## Navigation
* [v5 Migration guide](./docs/MIGRATION.md)
* [Dev/build scripts](#markdown-header-devbuild-scripts)
    * [Usage](#markdown-header-usage)
* [Assets handling](#markdown-header-assets-handling)
* [Serving static assets not handled by webpack](#markdown-header-serving-static-assets-not-handled-by-webpack)
* [PROJECT_META constant](#markdown-header-project_meta-constant)
* [Bundle analyzer](#markdown-header-bundle-analyzer)
* [Config file resolving](#markdown-header-config-file-resolving)
    * [Babel](#markdown-header-babel)
    * [PostCSS](#markdown-header-postcss)
* [ESLint / StyleLint dependencies](#markdown-header-eslint-stylelint-dependencies)
* [Unit testing](#markdown-header-unit-testing)
* [Building packages](#markdown-header-building-packages)
    * [Normal packages](#markdown-header-normal-packages)
    * [Plugins](#markdown-header-plugins)
* [Web workers](#markdown-header-web-workers)
* [Extend webpack config](#markdown-header-extend-webpack-config)

## Dev/build scripts

Both scripts use webpack to resolve modules, handle assets, load Vue files, transpile JavaScript with Babel, compile .scss files, automatically prefix CSS rules, and more.

The development script spins up a webpack dev server with hot reload.

The build script creates a main and vendor bundle for JS/CSS files, versions assets, and minifies/optimizes everything.

### Usage

Add these scripts to your app's **package.json**:

```json
{
    "build": "node node_modules/bm-web-build/build/build-app.js",
    "report": "env config=localhost npm_config_report=true node node_modules/bm-web-build/build/build-app.js",
    "start": "webpack-dev-server --progress --config node_modules/bm-web-build/build/webpack.app-dev.conf.js"
}
```

For development, simply use `npm start` in a terminal. It will take some time to initially start up the server and prepare the project's assets. Every change you later make will either trigger in-place hot reload (e.g. for Vue components), or reload the app entirely.

To run the `build` script locally (for debugging purposes), you can specify that the environment is localhost, by default it's production, so run the script like this:

`env config=localhost npm run build`

As for the `report` script, see the later section on bundle analyzing.

## Aliases
// TODO:

## Assets handling

Webpack is set up to automatically include assets such as images in the build output. They will also be versioned. Files that are small enough will be inlined as base64 instead.

Bundling an asset is as simple as importing it:

```javascript
import image from './image.png';
```

Or referring to it from a component template:

```html
<img src="./image.png" alt="image" />
```

You can simply place these files inside your modules - for more generic assets like you can use assets folder and then use alias to import them: 

```html
<img src="@assets/image.png" alt="image" />
```
```css
div {
    background-image: url("@assets/image.png");
}
```

JSON files will be bundled with your code.

## Serving static assets not handled by webpack

Sometimes you want to ensure that your build output includes some files that Webpack can't or shouldn't handle, and serve those as static assets.

An example is favicons and other project meta files - we need these files included in the project root after building, but we currently don't have a way for Webpack to handle them.

Another example is large JSON files that you might want to load on demand instead of bundling them (async importing with webpack is being looked into).

For this purpose you can create a **static** directory in your project root. All files in this directory will be copied to the project root on build and served by the web server.

## Config file resolving

**bm-web-build** automatically builds your project with the correct configuration files based on environment (production by default).

Note: whenever possible, you should use **bm-config** for your configuration needs, as it scales better (configurations in the project source code should be avoided). The method described below is a fallback for more complex configs.

In your project root, create a **config** directory. Every file in here following a naming scheme of **config.name.js** will be made available to import as `@config/name`.

```javascript
import ModuleConfig from '@config/module'; // config.module.js
```

Files in the root of **config** serve as defaults. You can add environment-specific overrides in subdirectories. These should be named *localhost*, *staging* or *production*, or something pipedrive-specfic for custom builds (should be avoided - try using **bm-config** instead). To override a config from the root, simply create a config file with the same name in one of these directories.

Webpack will automatically resolve `@config/name` to the correct environment-specific file based on the current **config** environment variable. This is automatically set by the build server.

## PROJECT_META constant

You can use the global constant `PROJECT_META` to retrieve meta information about your app without having to import **package.json** (which would include it in the bundle, potentially leaking information not meant for users).

`PROJECT_META` is an object containing a `name` and `version` property.

## Bundle analyzer

**bm-web-build** includes webpack-bundle-analyzer, which you can use to see a graphical overview of your JavaScript bundles' dependencies. This is a good way to compare which dependencies are the largest when bundled, and can be useful for optimization.

To use, run the `report` script listed in an earlier section. This will open a webpage after building with an overview of your bundles.

## Configuration files

### Babel

Your app needs a Babel config for **bm-web-build** to work properly. A default configuration is included.

To use it, create a **babel.config.json** containing this:

```json
{
  "extends": "bm-web-build/babel.config.json"
}
```

You can configure which browsers to target with Babel by overriding the default **bm-web-build** config. Refer to **babel.config.json** and add the rules to your app's own config, then modify them as needed. See [browserslist](https://github.com/ai/browserslist) for syntax.

### PostCSS

Your app needs a PostCSS config so that **autoprefixer** can work correctly. It automatically prefixes CSS rules based on browser features.

Like with the Babel config, you can inherit from **bm-web-build**. Create a **postcss.config.js** file like this:

```javascript
module.exports = require('bm-web-build/postcss.config.js'); // eslint-disable-line import/no-commonjs
```

Configure the browsers your app targets in package.json using [browserslist](https://github.com/ai/browserslist)'s configuration method. Example:

```json
{
	"browserslist": [
		"last 2 chrome versions",
		"last 2 firefox versions",
		"last 2 safari versions",
		"last 2 edge versions"
	]
}
```

## ESLint / StyleLint dependencies

**bm-web-build** includes the ESLint and StyleLint packages needed for our linting setup, so you don't have to install these in your app.

Use the following scripts:

```json
{
	"lint": "npm run lint:code && npm run lint:css",
	"lint:code": "eslint . --ext '.js, .vue'",
}
```

For the `lint:css` script, check **stylelint-config-bemobile**'s documentation.

Linting is automatically done on the build server and should be ran locally as well during development.

You should have the configuration files listed in **eslint-config-bemobile** and **stylelint-config-bemobile**'s READMEs in your project root.

## Unit testing

**bm-web-build** includes unit testing with Karma and Jasmine.

Use the following scripts:

```json
{
	"unit": "cross-env BABEL_ENV=test karma start node_modules/bm-web-build/build/karma.conf.js",
	"test": "npm run lint && npm run unit -- --single-run "
}
```

The test folder should be located in the ./test/unit in the root of your project.
You can check vue-start-template-web for an example.

## Building packages

### Normal packages

We don't normally build our packages before publishing. Instead, we rely on the build process that builds our applications to also handle the modules exported by these packages. This speeds up development.

### Plugins

Some packages do have to be built before publishing. Applications that support a plugin architecture, with many different plugins available, should typically not rely on these plugins being installed as dependencies. Instead, they should be loaded at runtime from a static file server, based on an external configuration.

This greatly improves scalability and allows easy configuration per environment, by avoiding having to conditionally load each of these plugins in the source code of the application and needing to build a new version and redeploy the application for every new plugin that is developed.

In order to allow these plugin packages to be loaded on demand from a server, they should be built before publishing.

Add the following scripts to your package.json:

* Build a plugin so that it can be published: `"build": "node node_modules/bm-web-build/build/build-library.js"`
* Start a webpack-dev-server during development of the plugin: `"start": "webpack-dev-server --progress --config node_modules/bm-web-build/build/webpack.library-dev.conf.js"`

You should also install **bm-web-build** in your package.

## Web workers

Normally workers are handled by webpack https://webpack.js.org/guides/web-workers/#root
```javascript
const worker = new Worker(new URL('./worker.js', import.meta.url));
```

but some cases require using inline workers, for example, libraries cannot load workers due to CORS.
ATM there isn't a clear way of using inline workers with webpack features, and for this reason
webpack-loader was left. 

```javascript
// eslint-disable-next-line
import InlineWorker from 'worker-loader?inline=no-fallback!./inline-worker';
```

## Extend webpack config

Sometimes it could be handy to extend webpack config, for those purposes was created a mechanism 
which allows extending config per environment. You just need to put webpack config for desired env in the root of the project,
and it will be merged with config used by **bm-web-build** by webpack-merge. The following options are available:

* `webpack.config.js` will be merged with dev & prod configs
* `webpack.dev.config.js` will be merged only with dev config
* `webpack.prod.config.js` will be merged only with prod config
* `webpack.test.config.js` will be merged only with test config
